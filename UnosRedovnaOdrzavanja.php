<?php
require_once 'core/init.php';

if(Input::exists())
{
if(Token::check(Input::get('token')))

    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'tip_odrzavanja' => array(
            'required' => true,         
           
        ),
         'zaduzen_za_placanje' => array(
            'required' => true,
           
        ),
          'mjesecna_cijena' => array(
            'required' => true,
           
        )
                  
 ));
 
 if($validation->passed())
 {
     $redovna_odrzavanja = new Redovna_odrzavanja();
     
     try
     {
         
         $redovna_odrzavanja->create(array(
              'tip_odrzavanja' => Input::get('tip_odrzavanja'),
              'zaduzen_za_placanje' => Input::get('zaduzen_za_placanje'),
              'mjesecna_cijena' => Input::get('mjesecna_cijena')
             
       ));
       
         Session::flash('home', 'Uspjesno!');
     }
     catch(Exception $e)
     {
         die($e->getMessage());
     }
 } 
     else 
		{
			foreach($validation->errors() as $error)
			{
				echo $error, '<br';
			}
		}
 }


?>

<html>
    <head>
      </head>
<body>
        <form action='' method='post'>
       <div class="field">
            <label for="tip_odrzavanja">Tip održavanja</label>
            <input type="text" name="tip_odrzavanja" id="tip_odrzavanja" value="" autocomplete="off" />
          </div> 
          
            <div class="field">
            <label for="zaduzen_za_placanje">Zadužen za plaćanje</label>
            <input type="text" name="zaduzen_za_placanje" id="zaduzen_za_placanje" value="" autocomplete="off" />
          </div>
          
           <div class="field">
            <label for="mjesecna_cijena">Mjesečna cijena</label>
            <input type="text" name="mjesecna_cijena" id="mjesecna_cijena" value="" autocomplete="off" />
          </div>
          
          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
          <input type="submit" value="Submit">
          
          </form>
</body>
    
 </html>