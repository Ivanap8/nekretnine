<?php
require_once 'core/init.php';

if(!$username = Input::get('user'))
{
	Redirect::to('index.php');
}
else
{
	$db = new DB();
	$user = new User($username);

	if(!$user->exists())
	{
		Redirect::to(404);
	}
	else
	{
		$data = $user->data();
	}
	?>
	<a href="index.php">Natrag</a>
	<h3><?php echo escape($data->username); ?></h3>
	<p><b>Ime</b> : <?php echo escape($data->firstname. ' '); echo escape($data->lastname);?></p>
	<p><b>Drzava</b>: <?php echo escape($data->state . ' '); ?>
	<p><b>Grad</b> <?php echo escape($data->city); ?> </p>
	<p><b>Adresa</b> : <?php echo escape($data->adress. ' P.br '); echo escape($data->postnumber); ?></p>
	<p><b>Telefon </b>: <?php echo escape($data->phone); ?></p>
	<p><b>E-mail </b>: <?php echo escape($data->email); ?></p>
	<?php
	$nekretnina = new Nekretnina();
	$result = $db->getAll('nekretnina')->results();
	foreach($result as $res)
	{
		if($res->id_vlasnik == $data->id)
		{
			echo '<a href="profilnekretnina.php?nekretnina='. $res->ime .'"> ' . $res->ime . '</a><br>';
		}
	}
}
?>
