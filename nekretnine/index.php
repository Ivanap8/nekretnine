<?php

require_once 'core/init.php';

//$user = DB::getInstance()->get('users', array('username', '=', 'franjo'));

/*$user = DB::getInstance()->update('users', 3, array(
	'username' => 'Ivica',
	'password' => 'updatanasifra',
	));*/
if(Session::exists('home'))
{
 	//echo Session::flash('success');
	echo '<p>'. Session::flash('home') .'</p>';
}

$user = new User();

if($user->isLoggedIn())
{
?>
	<p>Pozdrav , dobrodosli na ProEstate <a href="profile.php?user=<?php echo escape($user->data()->username); ?>"><?php echo escape($user->data()->username); ?></a></p>

	<ul>
		<li><a href="logout.php">Logout</a></li>
		<li><a href="update.php">Update</a></li>
		<li><a href="changepassword.php">Promjena Lozinke</a></li>
		<li><a href="unosNekretnine.php">Dodaj Nekretninu</a></li>
	</ul>

	<?php 
		if($user->hasPermission('admin'))
		{
			echo 'Vi ste administrator <a href="admincentar.php">Admin Centar </a>';
		}

	?>
<?php
}
else
{
	echo '<p>Morate se prvo <a href="loginforma.php">prijaviti</a> ili <a href="registerforma.php">registrirati!</a></p>';
}
?>