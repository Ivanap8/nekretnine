<?php 
require_once 'core/init.php';

?>
<a href="index.php">Natrag</a><br>
<?php
$user = new User();
if(!$user->isLoggedIn())
{
  Redirect::to('index.php');
}

if(Input::exists())
{
if(Token::check(Input::get('token')))

    $validate = new Validate();
    $validation = $validate->check($_POST, array(
        'ime' => array(
            'required' => true,
            'unique' => 'nekretnina'
           
        ),
         'tip' => array(
            'required' => true,
           
        ),
          'povrsina' => array(
            'required' => true,
           
        ),
             'cijena_najma' => array(
            'required' => true,
           
        ),
             'godina_izgradnje' => array(
            'required' => true,
           
        ),
    
              'sadrzaj' => array(
            'required' => true,
           
        ),
        
 ));
 
 if($validation->passed())
 {
     $nekretnina = new Nekretnina();
     
     try
     {
          /*Proces samog kreiranje nekretnine u ovisnosti od unesenih podataka korisnika.*/
         $nekretnina->create('nekretnina', array(
              'ime' => Input::get('ime'),
              'id_vlasnik' => $user->data()->id,
              'tip' => Input::get('tip'),
              'povrsina' => Input::get('povrsina'),
              'cijena_najma' => Input::get('cijena_najma'),
              'godina_izgradnje' => Input::get('godina_izgradnje'),
              'sadrzaj' => Input::get('sadrzaj')
            ));
             /*$tnekretnina_lokacija->create(array(
                 'drzava' => Input::get('drzava'),
                  'grad' => Input::get('grad'),
                  'opcina' => Input::get('opcina'),
                  'adresa' => Input::get('adresa'),?*/
         Session::flash('home', 'Uspjesno!');
     }
     catch(Exception $e)
     {
         die($e->getMessage());
     }
 }

}
?>

<html>
  <head>
  </head>

  <body>
    <p><?php $user = new User(); echo escape($user->data()->username); ?></p>
      <form action ='' method='post'>

          <div class="field">
            <label for="ime">Ime Nekretnine</label>
            <input type="text" name="ime" id="ime" value="" autocomplete="off" />
          </div>

          <div class="field">
            <label for="tip">Tip Nekretnine</label>
            <input type="text" name="tip" id="tip" value="" autocomplete="off" />
          </div>

          <div class="field">
            <label for="povrsina">Povrsina Objekta</label>
            <input type="text" name="povrsina" id="povrsina" value="" autocomplete="off" />
          </div>

          <div class="field">
            <label for="cijena_najma">Cijena Najma</label>
            <input type="text" name="cijena_najma" id="cijena_najma" value="" autocomplete="off" />
          </div>

          <div class="field">
            <label for="godina_izgradnje">Godina Izgradnje</label>
            <input type="text" name="godina_izgradnje" id="godina_izgradnje" value="" autocomplete="off" />
          </div>

          <div class="field">
            <label for="sadrzaj">Sadrzaj</label>
            <input type="text" name="sadrzaj" id="sadrzaj" value="" autocomplete="off" />
          </div>

          <input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
          <input type="submit" value="Dodaj Nekretninu">
      </form>
  </body>
</html>
