<!DOCTYPE html>
<html>
<head>
	<title>ProEstate</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" rel="stylesheet" href="style/izgled.css"/>
	
</head>
<body>
	<div class="logo"> 
		<img src="style/img/ikona.png" />
	</div>

	<div class="sivi_kvadrat">
		<form action="login.php" method="post" accept-charset="UTF-8">
		<div class="korisnik-button">
			<input class="korisnik-button-unos" name="username" type="text" placeholder="Korisničko ime" autocomplete="off" />
		</div><br>

		<div class="lozinka-button">
			<input class="lozinka-button-unos" name="password" type="password" placeholder="Lozinka" autocomplete="off"/>
		</div>
		
		<input class="zap-prijava" type="checkbox" checked />
		<span class="zap-prijava-tekst">Zapamti moju prijavu </span>
		<a href="" class="zab-lozinka"> 
		<span class="zab-lozinka-tekst">Zaboravili ste lozinku? </span></a>
	
		<input type="hidden" name="token" value="<?php require_once 'core/init.php'; echo Token::generate(); ?>">
		<div>
			<input type="submit" value="Prijavi se" class="prijava prijava-tekst">
 		</div> 		
 		</form>
 		<div>
 			<form action="registerforma.php">
 			<input type="submit" value="Registriraj se" class="registracija registracija-tekst">
 			</form>
 		</div>
</div>
</body>
</html>
