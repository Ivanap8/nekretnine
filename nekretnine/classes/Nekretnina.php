<?php 
class Nekretnina
{
    private $_db,
            $_data;
     
      
	public function __construct($nekretnina = null)
	{
	    $this->_db = DB::getInstance();
	}

	public function create($table, $fields = array())
	{
		if(!$this->_db->insert($table, $fields))
		{
			throw new Exception('Greska!');
		}
	}

	public function find($nekretnina = null)
	{
		if($nekretnina)
		{
			$field = (is_numeric($nekretnina)) ? 'id' : 'ime';
			$data = $this->_db->get('nekretnina', array($field, '=', $nekretnina));

			if($data->count())
			{
				$this->_data = $data->first();
				return true;
			}
		}
		return false;
	}

	public function exists()
	{
		return (!empty($this->_data)) ? true : false;
	}


	public function data()
	{
		return $this->_data;
	}

	/*public function getAll()
   	{
   		$data = $this->_db->get('nekretnina', array('ime', '=', 'ime'));
   		$results = $this->_db->query($data); 
   		foreach ($results as $nekretnina)
		{
  			echo $user["ime"];
		}
   	}*/
}
