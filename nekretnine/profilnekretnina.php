<?php
require_once 'core/init.php';
?>
<a href="index.php">Natrag</a><br>

<?php
if(!$idnekretnina = Input::get('nekretnina'))
{
	Redirect::to('index.php');
}
else
{
	$db = new DB();
	$user = new User();
	$nekretnina = new Nekretnina($idnekretnina);
	$nekretnina->find($idnekretnina);

	if(!$nekretnina->exists())
	{
		Redirect::to(404);
	}
	else
	{
		$data = $nekretnina->data();
	}
	?>
	<h3><?php echo escape($data->ime); ?></h3>
	<p><b>Povrsina</b> : <?php echo escape($data->povrsina); ?> m2</p>
	<p><b>Cijena Najma</b> : <?php echo escape($data->cijena_najma); ?> €</p>
	<p><b>Godina Izgradnje</b> : <?php echo escape($data->godina_izgradnje); ?></p>
	<p><b>Opis</b> : <?php echo escape($data->sadrzaj); ?></p>
	<?php
	$result = $db->getAll('users')->results();
	foreach($result as $res)
	{
		if($data->id_vlasnik == $res->id)
		{
			echo '<b>Vlasnik</b> : '. $res->username;
		}
	}
	?>

	<p><b>Iznajmljeno</b> : Ne</p>
	<form action="" method="post">
		<input type="text" name="najam" placeholder="Unesite korisnika">
		<input type="submit" name="najam-sub">
	</form>
	<?php

	if(Input::get('najam'))
	{
		$najam = Input::get('najam');
		if($user->find($najam))
		{
			//$nekretnina->update(array("iznajmljeno" => 1));
			//$nekretnina->update(array('id_najam' => $data->id), $najam);
			echo $najam;
		}
		else
		{
			echo 'Korisnik ne postoji!';
		}
	}
	/*Listanje svih troskova po mjesecu za pojedinu nekretninu*/
	$resultTroskovi = $db->getAll('troskovi')->results();
	foreach($resultTroskovi as $troskovi)
	{
		if($data->id == $troskovi->id_nekretnine)
		{
			echo '<b>Mjesec</b> :' . $troskovi->mjesec .'<br>';
			echo '<b>Struja</b> : '. $troskovi->struja .'<br>';
			echo '<b>Voda</b> : ' . $troskovi->voda .'<br>';
			echo '<b>Telefon</b> :' . $troskovi->telefon. '<br>';
		}
	}
	echo '<a href="opcijenekretnine.php?nekretnina='. $data->ime .'">Opcije vase Nekretnine</a><br>';
		
}
?>