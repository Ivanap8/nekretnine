<?php
$servername = "localhost";
$databasename = "nekretnine";
$username = "root";
$password = "";

try
{
	$connection = new PDO("mysql:host=$servername;dbname=$databasename", $username, $password);
	$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	echo 'Connection succed';
} catch(PDOException $e)
{
	echo 'Failed';
}
?>