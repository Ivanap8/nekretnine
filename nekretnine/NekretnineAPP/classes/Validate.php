<?php
class Validate
{
	/*Spojili smo se na bazu , napravili funkciju check koja provjera sva pravila 
	kad napravimo u register.php poslja ova klasa provjera dali sve ispravno radi dali su zadovoljeni uvijeti
	koji su takodjer i u databazi jednaki*/
	private $_passed = false,
			$_errors = array(),
			$_db = null;

	public function __construct()
	{
		$this->_db = DB::getInstance();
	}

	public function check($source, $items = array())
	{
		foreach($items as $item => $rules)
		{
			foreach($rules as $rule => $rule_value)
			{
				$value = trim($source[$item]);
				$item = escape($item);

				if($rule === 'required' && empty($value))
				{
					$this->addError("{$item} is required");
				}
				else if(!empty($value))
				{
					//Sva pravila 
					switch ($rule) 
					{
						case 'min':
							if(strlen($value) < $rule_value)
							{
								$this->addError("{$item} mora imat minimalno {$rule_value} znakova!");
							}
							break;
						case 'max':
							if(strlen($value) > $rule_value)
							{
								$this->addError("{$item} mora imat maximalno {$rule_value} znakova!");
							}
							break;
						case 'matches':
							if($value != $source[$rule_value])
							{
								$this->addError("{$rule_value} mora biti isto kao {$item}");
							}
							break;
						case 'unique':
							$check = $this->_db->get($rule_value, array($item, '=', $value));

							if($check->count())
							{
								$this->addError("{$item} netko vec koristi to ime!");
							}
							break;
						
						default:
							# code...
							break;
					}
				}
			}
		}
		if(empty($this->_errors))
		{
			$this->_passed = true;
		}
		return $this;
	}

	private function addError($error)
	{
		$this->_errors[] = $error;
	}

	public function errors()
	{
		return $this->_errors;
	}

	public function passed()
	{
		return $this->_passed;
	}
}

?>