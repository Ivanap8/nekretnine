<?php
/*Kreirane metode za vadit podatke preko OOPa umjesto
naprimjer $item = $_POST['item'];
podatke dobavljamo preko $item = Input::get('item');

*/
class Input
{
	//Ako nepostavimo metodu exists na GET ili POST automacki bira za nas POST
	public static function exists($type = 'post')
	{
		switch ($type) {
			case 'post':
				return (!empty($_POST)) ? true : false;
				break;
			case 'get':
				return (!empty($_GET)) ? true : false;
				break;
			default:
				return false;
				break;
		}
	}

	public static function get($item)
	{
		if(isset($_POST[$item]))
		{
			return $_POST[$item];
		}
		else if(isset($_GET[$item]))
		{
			return $_GET[$item];
		}
		return '';
	}

}
?>