<?php

class Config
{
	// Metoda kojom izvlacimo podatke iz init.php da bi mogli 
	// Napravit neku putanju i tako povlacit podatke iz nje...
	public static function get($path = null)
	{
		if($path)
		{
			$config = $GLOBALS['config'];
			$path = explode('/', $path);

			foreach($path as $bit)
			{
				if(isset($config[$bit]))
				{
					$config = $config[$bit];
				}
			}

			return $config;
		}

		return false;
	}
}

?>