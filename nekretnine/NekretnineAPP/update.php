<?php
require_once 'core/init.php';

$user = new User();

if(!$user->isLoggedIn())
{
	Redirect::to('index.php');
}

if(Input::exists())
{
	if(Token::check(Input::get('token')))
	{
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'firstname' => array(
				'required' => true,
				'min' => 2
			)
		));

		if($validation->passed())
		{
			try
			{
				$user->update(array(
					'name' => Input::get('firstname')
				));

				Session::flash('home', 'Update uradjen!');
				Redirect::to('index.php');
			} 
			catch(Exception $e)
			{
				die($e->getMessage());
			}
		}
		else
		{
			foreach($validation->errors() as $error)
			{
				echo $error, '<br>';
			}
		}
	}
}
?>

<form action="" method="post">
	<div class="field">
		<label for="firstname">Vas ime </label>
		<input type="text" id="firstname" value="<?php echo escape($user->data()->name); ?>" name="firstname" />
			
		<input type="submit" value="Update">
		<input type="hidden" name="token" value="<?php echo Token::generate(); ?>">
	</div>
</form>
