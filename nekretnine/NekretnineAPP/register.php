<?php
require_once 'core/init.php';

if(Input::exists())
{
	if(Token::check(Input::get('token')))
	{
	
		//Poziva se validate.php koja provjera dali je sve ispravno 
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'users'
			),
			'password' => array(
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),
			'firstname' => array(
				'required' => true,
				'min' => 2
			)
		));

		if($validation->passed())
		{
			$user = new User();

			$salt = Hash::salt(32);
			
			try
			{
				$user->create(array(
					'username' => Input::get('username'),
					'password' => Hash::make(Input::get('password'), $salt),
					'salt' => $salt,
					'name' => Input::get('firstname'),
					'joined' => date('Y-m-d H:i:s'),
					'group' => 1
					));
				Session::flash('home', 'Uspjesno ste registrirani!');
				Redirect::to('index.php');
			}
			catch(Exception $e)
			{
				die($e->getMessage());
			}
		}
		else 
		{
			foreach($validation->errors() as $error)
			{
				echo $error, '<br';
			}
		}
	}
}
?>

<html>
	<head>
	</head>

	<body>
		<form action='' method='post'>
			<div class="field">
				<label for="username">Username</label>
				<input type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off" />
			</div>

			<div class="field">
				<label for="password">Password</label>
				<input type="password" id="password" name="password" />
			</div>

			<div class="field">
				<label for="password_again">Repeat password</label>
				<input type="password" id="password_again" name="password_again" />
			</div>

			<div class="field">
				<label for="firstname">Vas ime </label>
				<input type="text" id="firstname" value="<?php echo escape(Input::get('firstname')); ?>" name="firstname" />
			</div>

			<input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
			<input type="submit" value="Register" />
		</form>
	</body>
</html>