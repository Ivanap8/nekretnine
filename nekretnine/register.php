<?php
require_once 'core/init.php';

if(Input::exists())
{
	if(Token::check(Input::get('token')))
	{
	
		//Poziva se validate.php koja provjera dali je sve ispravno 
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			'username' => array(
				'required' => true,
				'min' => 2,
				'max' => 20,
				'unique' => 'users'
			),
			'password' => array(
				'required' => true,
				'min' => 6
			),
			'password_again' => array(
				'required' => true,
				'matches' => 'password'
			),
			'firstname' => array(
				'required' => true,
				'min' => 2
			),
			//Nove informacije
			'lastname' => array(
				'required' => true,
				'min' => 2
			),
			'company' => array(
				'required' => true,
				'min' => 2
			),
			'adress' => array(
				'required' => true,
				'min' => 5
			),
			'postnumber' => array(
				'required' => true,
				'min' => 1
			),
			'city' => array(
				'required' => true,
				'min' => 2
			),
			'state' => array(
				'required' => true,
				'min' => 2
			),
			'phone' => array(
				'required' => true,
				'min' => 6
			),
			'email' => array(
				'required' => true,
				'min' => 2
			),
			'repeatemail' => array(
				'required' => true,
				'matches' => 'email'
			),
		));

		if($validation->passed())
		{
			$user = new User();

			$salt = Hash::salt(32);
			
			try
			{
				$user->create(array(
					'username' => Input::get('username'),
					'password' => Hash::make(Input::get('password'), $salt),
					'salt' => $salt,
					'firstname' => Input::get('firstname'),
					'lastname' => Input::get('lastname'),
					'company' => Input::get('company'),
					'adress' => Input::get('adress'),
					'postnumber' => Input::get('postnumber'),
					'city' => input::get('city'),
					'state' => Input::get('state'),
					'phone' => Input::get('phone'),
					'email' => Input::get('email'),
					'joined' => date('Y-m-d H:i:s'),
					'group' => 1
					));
				Session::flash('home', 'Uspjesno ste registrirani!');
				Redirect::to('index.php');
			}
			catch(Exception $e)
			{
				die($e->getMessage());
			}
		}
		else 
		{
			foreach($validation->errors() as $error)
			{
				echo $error, '<br';
			}
		}
	}
}
?>
