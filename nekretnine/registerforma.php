<!DOCTYPE html>
<?php
require_once 'core/init.php';
?>
<html lang="en">
<head>
    <title>ProEstate</title>
    <meta charset="UTF-8">
    <link href="style/stil1.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<section>
		<div class="pozadina">
		<img src="style/img/pozadinareg.jpg" />
		</div>
	</section>
	<section>
		<div class="orange_kvadrat">
		</div>
	</section>
	<section>
		<div class="sivi_kvadrat">
		<form action="register.php" method="post">
		<div class="button-unos1">
		<span class="tekst-button1"> Ime </span>
		<input class="korisnik-button-unos" type="text" id="firstname" value="<?php echo escape(Input::get('firstname')); ?>" name="firstname" />
		</div>
		<div class="button-unos2">
		<span class="tekst-button2"> Prezime </span>
		<input class="korisnik-button-unos" type="text" id="lastname" value="<?php echo escape(Input::get('lastname')); ?>" name="lastname" />
		</div>
		<div class="button-unos3">
		<span class="tekst-button3"> Tvrtka </span>
		<input class="korisnik-button-unos" type="text" id="company" value="<?php echo escape(Input::get('company')); ?>" name="company" />
		</div>
		<div class="button-unos4">
		<span class="tekst-button4"> Adresa </span>
		<input class="korisnik-button-unos" type="text" id="adress" value="<?php echo escape(Input::get('adress')); ?>" name="adress"/>
		</div>
		<div class="button-unos5">
		<span class="tekst-button5"> Poštanski broj </span>
		<input class="korisnik-button-unos" type="text" id="postnumber" value="<?php echo escape(Input::get('postnumber')); ?>" name="postnumber" />
		</div>
		<div class="button-unos6">
		<span class="tekst-button6"> Grad </span>
		<input class="korisnik-button-unos" type="text" id="city" value="<?php echo escape(Input::get('city')); ?>" name="city" />
		</div>
		<div class="button-unos7">
		<span class="tekst-button7"> Država </span>
		<input class="korisnik-button-unos" type="text" id="state" value="<?php echo escape(Input::get('state')); ?>" name="state" />
		</div>
		<div class="button-unos8">
		<span class="tekst-button8"> Telefon </span>
		<input class="korisnik-button-unos" type="text" id="phone" value="<?php echo escape(Input::get('phone')); ?>" name="phone"/>
		</div>
		<div class="button-unos9">
		<span class="tekst-button9"> E-mail </span>
		<input class="korisnik-button-unos" type="text" id="email" value="" name="email"/>
		</div>
		<div class="button-unos10">
		<span class="tekst-button10"> Ponovi E-mail </span>
		<input class="korisnik-button-unos" type="text" id="repeatemail" value="" name="repeatemail" />
		</div>
		<div class="button-unos11">
		<span class="tekst-button11"> Korisničko ime </span>
		<input class="korisnik-button-unos1" type="text" name="username" id="username" value="<?php echo escape(Input::get('username')); ?>" autocomplete="off"/>
		</div>
		<div class="button-unos12">
		<span class="tekst-button12"> Lozinka </span>
		<input class="korisnik-button-unos1" type="password" name="password" id="password" autocomplete="off"/>
		</div>
		<div class="button-unos13">
		<span class="tekst-button13"> Ponoviti lozinku </span>
		<input class="korisnik-button-unos1" type="password" id="password_again" name="password_again"/>
		</div>
		<div class="button-unos14">
		<span class="tekst-button14"> Sigurnosni kod </span>
		<input class="korisnik-button-unos1" type="text" />
		</div>
		<input type="hidden" name="token" value="<?php echo Token::generate(); ?>" />
		<input class="registracija" type="submit" value="Register" />
		
		<span class="tekst-dno"> Klikom na tipku Registriraj se, prihvaćate naše <span class="tekst-dno-bold">Uvjete</span> i potvrđujete da ste pročitali naša <span class="tekst-dno-bold">Pravila o upotrebi podataka </span></span>
		</form>
	</section>
</body>
</html>